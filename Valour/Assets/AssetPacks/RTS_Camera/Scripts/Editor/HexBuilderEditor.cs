using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(HexGridBuilder))]
public class HexBuilderEditor : Editor
{

    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();

        HexGridBuilder gridBuilder = (HexGridBuilder)target;

        if (GUILayout.Button("Build Grid"))
        {
            gridBuilder.BuildGrid();
        }
        if (GUILayout.Button("Destroy Grid"))
        {
            gridBuilder.DestroyGrid();
        }
    }

}
