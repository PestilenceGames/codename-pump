using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.Events;

public class InputManager : MonoBehaviour
{
    public static InputManager Instance;

    [Header("Navigation Tooltip")]
    [SerializeField] private Camera mainCam;
    [SerializeField] private LayerMask gridLayer;
    [SerializeField] private LayerMask towerLayer;
    [SerializeField] private Transform cursor;
    // [SerializeField] private GameObject keepUI;
    public UnityEvent OnDisplayKeep;
    private Ray camToWorld;
    private RaycastHit gridHit;
    [HideInInspector]
    public ITower currentTower;
    // TODO Set back to private
    private TowerType currentTowerBrain;
    private ITower hitTower;
    private Vector3 gridHitPoint;

    private void Awake()
    {
        Instance = this;
    }


    #region //Cursor

    public void ScanWorldForGridCursorPlacement()
    {
        camToWorld = mainCam.ScreenPointToRay(Input.mousePosition);

        if (Physics.Raycast(camToWorld, out gridHit, 50f, gridLayer, QueryTriggerInteraction.Collide))
        {
            gridHitPoint = new Vector3(gridHit.transform.position.x, 0.1f, gridHit.transform.position.z);
            cursor.position = gridHitPoint;
        }
        else
        {
            gridHitPoint = Vector3.zero;
            HideCursor();
        }
    }

    private void HideCursor()
    {
        cursor.position = Vector3.zero;
    }

    public ITower ScanWorldForTower()
    {
        hitTower = null;

        if (Physics.Raycast(camToWorld, out gridHit, 50f, towerLayer, QueryTriggerInteraction.Collide))
        {
            // Show keep UI
            if (gridHit.transform.CompareTag("Keep"))
                OnDisplayKeep.Invoke();

            UpgradesUIManager.Instance.UpdateSelectedTower(gridHit.transform.GetComponent<TowerAI>());
            // I know this .parent is bad practice, I included it to cut down on hard references in a hierarchy unlikely to change

            hitTower = gridHit.transform.GetComponent<ITower>();
        }
        else
            UpgradesUIManager.Instance.UpdateSelectedTower(null);


        return hitTower;

    }

    private bool IsMouseOverUI()
    {
        return EventSystem.current.IsPointerOverGameObject();
    }

    private void FixedUpdate()
    {
        if (!IsMouseOverUI())
            ScanWorldForGridCursorPlacement();
        else
            HideCursor();


    }

    private void Update()
    {
        if (Input.GetMouseButtonDown(0) && !IsMouseOverUI())
        {
            currentTower = ScanWorldForTower();
        }

        if (Input.GetMouseButtonDown(1))
        {
            if (currentTower != null && gridHitPoint != Vector3.zero)
            {
                currentTower.SetDestination(cursor.position);
            }
        }


    }

    #endregion



}
