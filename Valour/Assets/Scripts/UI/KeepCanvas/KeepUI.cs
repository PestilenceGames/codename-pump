using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class KeepUI : MonoBehaviour
{

    [SerializeField] private GameObject keepCanvas;
    [SerializeField] private Transform keep;
    [SerializeField] private Camera mainCam;
    public UnityEvent OnSpawnUnit;
    private int baseCost;

    private void Start()
    {
        DisableKeepCanvas(); 
        baseCost = TowerManager.Instance.GetStatsOfType(TowerType.Base).cost;
    }

    public void DisableKeepCanvas()
    {
        keepCanvas.SetActive(false);
    }

    public void DisplayKeepCanvas()
    {
        keepCanvas.SetActive(true);
        Vector3 _uiPos = mainCam.WorldToScreenPoint(keep.transform.position);
        keepCanvas.transform.position = _uiPos;
    }

    public void SpawnUnit()
    {
        if (EconomyManager.Instance.CanThisBeAfforded(baseCost) && TowerManager.Instance.CanSpawnTower())
        {
            EconomyManager.Instance.PurchaseThis(baseCost);
            OnSpawnUnit.Invoke();
            DisableKeepCanvas();        
        }
    }


}
