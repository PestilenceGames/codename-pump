using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class EconomyUI : MonoBehaviour
{
    public static EconomyUI Instance;

    [SerializeField] private TextMeshProUGUI goldText;

    private void Awake()
    {
        Instance = this;
    }

    public void DisplayGold(int amount)
    {
        goldText.text = amount.ToString();
    } 

}
