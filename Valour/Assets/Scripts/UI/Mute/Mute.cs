using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Mute : MonoBehaviour
{

    [SerializeField] private GameObject sound;

    public void ToggleMute()
    {
        if (sound.activeSelf)
            sound.SetActive(false);
        else
            sound.SetActive(true);
    }

}
