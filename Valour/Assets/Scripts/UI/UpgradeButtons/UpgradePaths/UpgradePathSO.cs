using UnityEngine;
using System.Collections.Generic;

[CreateAssetMenu(fileName = "UpgradePathSO", menuName = "UpgradePathSO", order = 0)]
public class UpgradePathSO : ScriptableObject
{
    public TowerType type;
    public int level = 1;
    public List<TowerType> activeButtons = new List<TowerType>();

    public List<TowerType> upgradePath = new List<TowerType>();

}