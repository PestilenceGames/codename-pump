using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
public class ButtonScript : MonoBehaviour
{
    public TowerType type;

    [SerializeField] private GameObject locked;
    [SerializeField] private GameObject unLocked;
    [SerializeField] private GameObject text;
    [SerializeField] private Transform active;
    [SerializeField] private int level = 1;
    [SerializeField] private TextMeshProUGUI costText;
    private TowerTypeSO tempTower;
    private bool clickable = false;
    private int cost;

    private void Start()
    {
        costText.text = "";
        tempTower = TowerManager.Instance.GetStatsOfType(type);

        cost = tempTower.cost;

    }

    public void Lock()
    {
        costText.text = "";
        Disable();
        locked.SetActive(true);
        unLocked.SetActive(false);
        text.SetActive(false);
        active.gameObject.SetActive(false);
    }

    private void Disable()
    {
        clickable = false;
        // Button.enabled would be better for feedback, but caused a bug I couldn't fix.
    }

    public void EnableClickable()
    {
        clickable = true;
        costText.text = cost.ToString();
        active.gameObject.SetActive(true);
    }

    public void UnLock()
    {
        locked.SetActive(false);
        unLocked.SetActive(true);
        text.SetActive(true);
    }

    public void OnButtonPress()
    {
        if (!locked.activeSelf && clickable)
        {
            tempTower = TowerManager.Instance.GetStatsOfType(type);
            int _cost = (level == 1) ? tempTower.cost : tempTower.cost;

            if (EconomyManager.Instance.CanThisBeAfforded(_cost))
            {
                UpgradesUIManager.Instance.UpgradeSelectedUnit(type);
                EconomyManager.Instance.PurchaseThis(_cost);
            }
            else
                Debug.Log("Remember to add can't afford cost");
            // TODO add cannot afford prompt
        }
    }
}
