using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LoseUI : MonoBehaviour
{

    [SerializeField] private GameObject loseScreen;
    [SerializeField] private string mainMenuName;
    [SerializeField] private string gameSceneName;

    private void Start()
    {
        loseScreen.SetActive(false);
    }

    public void DisplayLoseScreen()
    {
        loseScreen.SetActive(true);
    }

    public void QuitToMainMenu()
    {
        SceneManager.LoadScene(mainMenuName, LoadSceneMode.Single);
    }

    public void Restart()
    {
        SceneManager.LoadScene(gameSceneName, LoadSceneMode.Single);
    }

    public void QuitGame()
    {
        Application.Quit();
    }

}
