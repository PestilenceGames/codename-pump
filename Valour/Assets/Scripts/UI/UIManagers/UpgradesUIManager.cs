using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UpgradesUIManager : MonoBehaviour
{
    public static UpgradesUIManager Instance;
    [SerializeField] private GameObject upgradeCanvas;
    [SerializeField] private GameObject upgradeButton;
    // [SerializeField] private TowerBrain currentTower;
    [SerializeField] private TowerAI currentTower;

    [Header("Upgrade Path SO")]
    [SerializeField] private List<UpgradePathSO> upgradePaths = new List<UpgradePathSO>();
    [SerializeField] private ButtonScript standard;

    [SerializeField] private List<ButtonScript> allPathButtons = new List<ButtonScript>();
    private UpgradePathSO tempUpgradeSo;
    private List<TowerType> activeButtons = new List<TowerType>();


    private void Awake()
    {
        Instance = this;
    }

    private void Start()
    {
        HideUICanvas();
        upgradeButton.SetActive(false);
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            UpgradeSelectedUnit(TowerType.Base);
            // TODO Remove cheat
        }
    }

    #region  Canvas Manipulation

    public void ToggleUpgradeCanvas()
    {
        if (upgradeCanvas.activeSelf)
            HideUICanvas();
        else
            ShowUICanvas();
    }

    public void ShowUICanvas()
    {
        upgradeCanvas.SetActive(true);
        DisplayUpgradeUI();
    }

    public void HideUICanvas()
    {
        upgradeCanvas.SetActive(false);
    }

    public void UpdateSelectedTower(TowerAI selectedTower)
    {
        currentTower = selectedTower;

        if (currentTower != null)
            upgradeButton.SetActive(true);
        else
            upgradeButton.SetActive(false);
    }

    #endregion

    #region  Upgrades

    public void DisplayUpgradeUI()
    {
        tempUpgradeSo = null;

        LockAllButtons();
        // TODO find this tempUpgradePath
        foreach (var upgrade in upgradePaths)
        {
            if (upgrade.type == currentTower.type)
                tempUpgradeSo = upgrade;
        }

        if (tempUpgradeSo != null)
        {
            // Debug.Log($"Trying to display {currentTower.thisTowerType}");
            activeButtons = tempUpgradeSo.activeButtons;
            UnlockButtonsFromList(activeButtons);
            DrawLines();
        }
        else
            Debug.LogWarning("No upgrade path found");
    }

    private void DrawLines()
    {
        // TODO Connect active dots with a line renderer
    }

    private void UnlockButtonsFromList(List<TowerType> buttons)
    {
        foreach (ButtonScript _button in allPathButtons)
        {
            foreach (TowerType _unLock in buttons)
            {
                if (_button.type == _unLock)
                {
                    _button.UnLock();

                    // Debug.Log("Button type " + _button.type);
                    // Debug.Log("Tower type " + currentTower.thisTowerType);
                    // Debug.Break();

                    if ((int)_button.type > (int)currentTower.type)
                    {
                        _button.EnableClickable();
                    }
                }

            }
        }
    }

    public void LockAllButtons()
    {
        foreach (ButtonScript _button in allPathButtons)
        {
            _button.Lock();
        }
        standard.UnLock();
    }

    public void UpgradeSelectedUnit(TowerType _type)
    {
        if (currentTower != null)
        {
            Debug.Log($"Set {currentTower} to {_type}");
            currentTower.SetTowerToType(_type);
            DisplayUpgradeUI();
        }
        else
            Debug.LogWarning("The selected unit is missing and cannot be upgraded");

    }
    #endregion

}
