using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class GameManager : MonoBehaviour
{

    public static GameManager Instance;
    public bool gamelost;
    public UnityEvent OnLoose;

    private void Start()
    {
        Instance = this;
        gamelost = false;
    }

    public void LoseState()
    {
        OnLoose.Invoke();
    }

}