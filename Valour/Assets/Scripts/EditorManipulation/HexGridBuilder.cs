using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HexGridBuilder : MonoBehaviour
{

    [SerializeField] private GameObject hex;

    [Header("Hex Settings")]
    [SerializeField] private int gridWidth;
    [SerializeField] private int gridHeight;
    [SerializeField] private Vector3 startPos;

    [Header("Hex Qualities")]
    [SerializeField] private float edgeLength;
    [SerializeField] private float spacing;

    private float rowOffset;
    private float heightOffset;
    private Vector3 currentPosition;
    private Transform gridParent;
    private GameObject newHex;

    public void BuildGrid()
    {
        Debug.Log("building...");

        CalculateWidthAndHeightOffset();

        gridParent = null;
        gridParent = new GameObject("Grid Parent").transform;

        for (var h = 0; h < gridHeight; h++)
        {
            if (h % 2 == 0)
            {
                for (var r = 0; r < gridWidth; r++)
                {
                    newHex = Instantiate(hex, startPos + new Vector3(rowOffset * r, 0, 0) + new Vector3(0, 0, heightOffset * h), hex.transform.rotation);
                    newHex.transform.parent = gridParent;
                }
            }
            else
            {
                for (var r = 0; r < gridWidth; r++)
                {
                    newHex = Instantiate(hex, (startPos + new Vector3(0.5f * rowOffset , 0, 0)) + new Vector3(rowOffset * r, 0, 0) + new Vector3(0, 0, heightOffset * h), hex.transform.rotation);
                    newHex.transform.parent = gridParent;
                }

            }
        }
    }

    public void DestroyGrid()
    {
        if (gridParent != null)
        {
            DestroyImmediate(gridParent.gameObject);
        }
    }

    private void CalculateWidthAndHeightOffset()
    {
        rowOffset = (Mathf.Sqrt(3) * edgeLength) + spacing;

        heightOffset = (1.5f * edgeLength) + spacing;
    }
}
