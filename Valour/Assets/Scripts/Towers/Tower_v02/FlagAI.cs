using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FlagAI : MonoBehaviour
{
    [SerializeField] private TowerAI thisTowerAI;
    private TowerAI collidedTower;

    public void SetUpFlag(TowerAI _thisTowerAI)
    {
        thisTowerAI = _thisTowerAI;
    }

    private void OnTriggerEnter(Collider other)
    {
        collidedTower = other.transform.GetComponent<TowerAI>();
        if (collidedTower != null && collidedTower == thisTowerAI)
        {
            collidedTower.DestinationReached();
        }
    }
}
