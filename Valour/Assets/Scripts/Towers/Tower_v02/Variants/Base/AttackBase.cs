using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AttackBase : MonoBehaviour
{
    public Transform projectileSpawn;
    public Transform projectile;

    public ZombieBase target;
    public ZombieBase tempZombie;
    public float damage;
    
    private void OnEnable()
    {
        if (projectile != null)
            projectile.gameObject.SetActive(false);
    }

    public virtual void StartUp()
    {

    }

    public virtual void AttackSequence(ZombieBase _target, float _damage)
    {
        damage = _damage;
        target = _target;
    }

    public virtual void Hit(Vector3 location)
    {

    }

}
