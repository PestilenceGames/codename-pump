using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Archer : AttackBase
{

    public override void StartUp()
    {

    }

    public override void AttackSequence(ZombieBase target, float damage)
    {
        base.AttackSequence(target, damage);

        projectile.gameObject.SetActive(true);
        projectile.GetComponent<Projectile>().FireAt(projectileSpawn.position, target.transform, this);
    }

    public override void Hit(Vector3 location)
    {
        base.Hit(location);

        target.TakeDamage(damage);

    }

}
