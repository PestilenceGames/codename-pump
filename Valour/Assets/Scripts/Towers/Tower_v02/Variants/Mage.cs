using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Mage : AttackBase
{
    [SerializeField] private float explosionRange;
    [SerializeField] private Transform particles;
    private ParticleSystem magicStars;

    public override void StartUp()
    {
        magicStars = particles.GetComponent<ParticleSystem>();
    }

    public override void AttackSequence(ZombieBase target, float damage)
    {
        base.AttackSequence(target, damage);

        projectile.gameObject.SetActive(true);
        projectile.GetComponent<Projectile>().FireAt(projectileSpawn.position, target.transform, this);
    }

    public override void Hit(Vector3 location)
    {
        particles.position = location;
        magicStars.Play();

        base.Hit(location);
        Collider[] targetColliders = Physics.OverlapSphere(location, explosionRange, EnemyManager.Instance.zombieLayer);

        foreach (Collider zombie in targetColliders)
        {
            tempZombie = zombie.transform.GetComponent<ZombieBase>();

            if (tempZombie != null)
                tempZombie.TakeDamage(damage);
        }
    }
}
