using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Projectile : MonoBehaviour

{
    [SerializeField] private float moveSpeed = 20f;
    [SerializeField] private float updateFrequency = 0.05f;
    private Vector3 moveDirection;
    private Transform destination;
    private AttackBase thisAttackBase;

    public void FireAt(Vector3 start, Transform _destination, AttackBase parent)
    {
        this.transform.position = start;
        destination = _destination;
        thisAttackBase = parent;

        // StartCoroutine(Travel());
    }

    private void FixedUpdate()
    {
        #region  From Brackeys

        if (destination != null)
        {
            moveDirection = (destination.position - transform.position);
            float distanceThisFrame = moveSpeed * Time.fixedDeltaTime;

            if (moveDirection.magnitude <= distanceThisFrame)
            {
                thisAttackBase.Hit(this.transform.position);
                this.gameObject.SetActive(false);
            }

            transform.Translate(moveDirection.normalized * distanceThisFrame, Space.World);
        }


        #endregion
    }


}
