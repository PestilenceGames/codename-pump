using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Melee : AttackBase
{
    [SerializeField] private Transform smack;
    private ParticleSystem hitParticle;
    public override void StartUp()
    {
        hitParticle = smack.GetComponent<ParticleSystem>();
    }

    public override void AttackSequence(ZombieBase target, float damage)
    {
        base.AttackSequence(target, damage);
        target.TakeDamage(damage);
        smack.position = target.transform.position;
        hitParticle.Play();
        
    }



}
