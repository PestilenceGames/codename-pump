using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AttackAI : MonoBehaviour
{

    [SerializeField] private List<ZombieBase> targets = new List<ZombieBase>();
    private TowerAI thisTowerAI;
    private SphereCollider attackSphere;
    private ZombieBase tempZombie;
    private ZombieBase currentTarget;
    private float currentRadius;
    private float attackCooldown;
    private bool onCooldown;

    public void InitializeSphere(float radius, float cooldown, TowerAI parent)
    {
        thisTowerAI = parent;

        attackSphere = this.transform.GetComponent<SphereCollider>();
        attackSphere.radius = radius;
        currentRadius = radius;

        attackCooldown = cooldown;
    }

    #region Managing Targets

    private void OnTriggerEnter(Collider other)
    {
        tempZombie = other.transform.GetComponent<ZombieBase>();

        if (tempZombie != null)
        {
            targets.Add(tempZombie);
            if (!onCooldown)
                LookForEnemiesToAttack();

        }
    }

    private void OnTriggerExit(Collider other)
    {
        tempZombie = other.transform.GetComponent<ZombieBase>();

        if (tempZombie != null)
        {
            RemoveTarget(tempZombie);
        }
    }

    public void RemoveTarget(ZombieBase target)
    {
        // Debug.Log("Removal Requested of " + target);

        for (var i = 0; i < targets.Count; i++)
        {
            if (targets.Contains(target))
                targets.Remove(target);
            else
                break;
        }
    }

    public void Pulse()
    {
        targets.Clear();
        attackSphere.enabled = true;

        LookForEnemiesToAttack();

        // Collider[] nearbyTargets = Physics.OverlapSphere(this.transform.position, currentRadius, EnemyManager.Instance.zombieLayer, QueryTriggerInteraction.Collide);
        // foreach (Collider zom in nearbyTargets)
        // {
        //     Debug.Log(zom);
        //     tempZombie = zom.transform.GetComponent<ZombieBase>();

        //     if (tempZombie != null && !tempZombie.Dead)
        //     {
        //         targets.Add(tempZombie);
        //     }
        // }
    }

    public void NullRadius()
    {
        attackSphere.enabled = false;
        targets.Clear();
    }

    #endregion

    #region  Attack

    public void LookForEnemiesToAttack()
    {
        StartCoroutine(WaitBeforeLooking());
    }

    IEnumerator WaitBeforeLooking()
    {
        yield return new WaitForSeconds(0.1f);

        if (targets.Count > 0 && !onCooldown)
        {
            AttackTarget();
        }
    }

    private void AttackTarget()
    {
        // Attack
        currentTarget = targets[0];

        thisTowerAI.AttackTarget(currentTarget);

        Cooldown();
    }

    private void Cooldown()
    {
        onCooldown = true;
        StartCoroutine(StartCooldown());
    }

    IEnumerator StartCooldown()
    {
        yield return new WaitForSeconds(attackCooldown);
        onCooldown = false;
        LookForEnemiesToAttack();
    }

    #endregion

}
