using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class TowerAI : HealthBase, ITower
{
    [Header("References")]
    [SerializeField] private AttackAI thisAttackAI;
    [SerializeField] private GameObject flagGO;
    [SerializeField] List<GameObject> variants = new List<GameObject>();

    [Header("This Tower")]
    public TowerType type;
    [SerializeField] private float attackRange;
    [SerializeField] private float attackDamage;
    [SerializeField] private float attackCooldown;
    [SerializeField] private NavMeshAgent thisAgent;
    [SerializeField] private AttackBase currentAttacker;

    [Header("Flag Settings")]
    private FlagAI thisFlag;

    #region  Privates
    private TowerTypeSO tempTowerTypeSO;
    private bool isMoving;
    #endregion

    // private void Update()
    // {
    //     if (Input.GetKeyDown(KeyCode.S))
    //     {
    //         SetTowerToType(TowerType.Mage);
    //         InputManager.Instance.currentTower = this;
    //     }
    //     // TODO Remove cheat
    // }

    public override void Die()
    {
        base.Die();
        this.gameObject.SetActive(false);
        TowerManager.Instance.ReplaceTower(this);
    }


    #region  Spawn
    private void OnEnable()
    {
        StartCoroutine(SpawnNextFrame());
    }

    public void Spawn(Vector3 position)
    {
        this.transform.position = position;

        SetUpDependencies();

        SetTowerToType();
    }

    IEnumerator SpawnNextFrame()
    {
        yield return new WaitForFixedUpdate();
        Spawn(this.transform.position);
    }

    private void SetUpDependencies()
    {
        // NavAgent
        thisAgent = this.transform.GetComponent<NavMeshAgent>();

        // Destination Flag
        thisFlag = flagGO.transform.GetComponent<FlagAI>();
        thisFlag.SetUpFlag(this);
        flagGO.SetActive(false);
    }

    #endregion

    #region Set Tower Type Overloads
    public void SetTowerToType()
    {
        SetTowerToType(TowerType.Base);
    }
    public void SetTowerToType(TowerType _type)
    {
        type = _type; 

        tempTowerTypeSO = TowerManager.Instance.GetStatsOfType(_type);

        maxHealth = tempTowerTypeSO.health;
        SetUpHealth();

        attackRange = tempTowerTypeSO.attackRange;
        attackDamage = tempTowerTypeSO.attackDamage;
        attackCooldown = tempTowerTypeSO.attackCooldown;

        thisAttackAI.InitializeSphere(attackRange, attackCooldown, this);

        DisableAllMeshButType(_type);
    }

    private void DisableAllMeshButType(TowerType type)
    {
        for (var i = 0; i < variants.Count; i++)
        {
            if (i == (int)type)
                variants[i].SetActive(true);
            else
                variants[i].SetActive(false);
        }

        if ((int)type > 7)
        {
            switch (type)
            {
                case TowerType.Archer02:
                    variants[(int)TowerType.Archer].SetActive(true);
                    break;
                case TowerType.Mage02:
                    variants[(int)TowerType.Mage].SetActive(true);
                    break;
                case TowerType.Infantry02:
                    variants[(int)TowerType.Infantry].SetActive(true);
                    break;
                case TowerType.Lancer02:
                    variants[(int)TowerType.Lancer].SetActive(true);
                    break;
                case TowerType.Knight02:
                    variants[(int)TowerType.Knight].SetActive(true);
                    break;
                default:
                    break;
            }
        }

        for (var i = 0; i < variants.Count; i++)
        {
            if (variants[i].activeSelf)
                currentAttacker = variants[i].transform.GetComponent<AttackBase>();
        }

        currentAttacker.StartUp();
    }

    #endregion

    #region Movement
    public void SetDestination(Vector3 destination)
    {
        thisAgent.destination = destination;
        flagGO.SetActive(true);
        flagGO.transform.position = destination;

        Moving = true;
    }

    public void DestinationReached()
    {
        flagGO.SetActive(false);
        Moving = false;
    }

    public bool Moving
    {
        get { return isMoving; }

        set
        {
            isMoving = value;

            if (!isMoving)
            {
                thisAttackAI.Pulse();
            }
            else
                thisAttackAI.NullRadius();  
        }
    }

    #endregion

    #region  Attack

    public void AttackTarget(ZombieBase target)
    {
        this.transform.LookAt(target.transform.position, Vector3.up);
        // TODO Turn transform slower

        currentAttacker.AttackSequence(target, attackDamage);
    }

    #endregion

}
