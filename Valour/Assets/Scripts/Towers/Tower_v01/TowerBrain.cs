using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TowerBrain : MonoBehaviour
{

    [Header("References")]
    [SerializeField] private TowerNav towerNav;
    [SerializeField] private VariantPicker variantPicker;
    
    // [HideInInspector]
    public TowerType thisTowerType;
    // [HideInInspector]
    public int thisTowerLevel;

    private void OnEnable()
    {
        Spawn();
    }

    public void Spawn()
    {
        thisTowerType = TowerType.Base;
        if (variantPicker != null)
        {
            variantPicker.SetupVariantPicker();
            SetTowerType(TowerType.Base, 1);
        }
        else
            Debug.LogWarning("Tower variant picker not found");
    }

    public void SetTowerType(TowerType type, int level)
    {
        thisTowerType = type;
        thisTowerLevel = level;
        variantPicker.TowerVariant = type;

        // variantPicker.SwitchTowerVariantTo(type);
    }


}

public enum TowerType
{
    Base,
    MeleeBase,
    RangedBase,
    Infantry,
    Lancer,
    Knight,
    Archer,
    Mage,
    Archer02,
    Mage02,
    Infantry02,
    Lancer02,
    Knight02
}
