using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface ITower
{
    public abstract void SetDestination(Vector3 destination);

}
