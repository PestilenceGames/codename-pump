using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class TowerNav : HealthBase, ITower
{
    // [Header("Variant Settings")]
    // [SerializeField] private VariantPicker variantPicker;

    // [Header("Health")]
    // [SerializeField] private int maxHealth;
    // [SerializeField] private int currentHealth;


    [Header("Navigation")]
    [SerializeField] private NavMeshAgent thisTowersAgent;
    [SerializeField] private Transform destinationFlag;
    private GameObject flagMesh;
    [SerializeField] private bool moving;
    private AttackPatternsBase thisAttackPattern;

    private void OnEnable()
    {
        thisTowersAgent = this.transform.GetComponent<NavMeshAgent>();
        if (destinationFlag != null)
        {
            flagMesh = destinationFlag.gameObject;
            SetupNavFlag();
        }
    }

    private void SetupNavFlag()
    {
        destinationFlag.GetComponent<FlagBrain>().SetUpNav(this);
    }

    public void DestinationReached()
    {
        flagMesh.SetActive(false);
        IsMoving = false;
    }

    public bool IsMoving
    {
        get { return moving; }

        set
        {
            moving = value;
            // thisAttackPattern.attackRing.enabled = !moving;

            if (thisAttackPattern != null)
            {
                thisAttackPattern.CanAttack = !moving;
                thisAttackPattern.UpdateTargets();
            }
            else
                Debug.LogWarning("No attackPattern Found for Moving");

        }
    }

    public void SetDestination(Vector3 destination)
    {
        thisTowersAgent.destination = destination;
        flagMesh.SetActive(true);
        destinationFlag.position = destination;
        IsMoving = true;
    }

    public override void Die()
    {
        // throw new NotImplementedException();
    }

    public void SetUpAttackPatternsBase(AttackPatternsBase request)
    {
        thisAttackPattern = request;
    }


}
