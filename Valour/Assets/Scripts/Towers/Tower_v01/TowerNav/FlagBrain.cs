using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FlagBrain : MonoBehaviour
{

    [SerializeField] private TowerNav thisTowerNavInstance;
    [SerializeField] private TowerNav collidedTower;

    public void SetUpNav(TowerNav towerNavInstance)
    {
        thisTowerNavInstance = towerNavInstance;
    }

    private void OnTriggerEnter(Collider other)
    {
        collidedTower = other.transform.GetComponent<TowerNav>();
        if (collidedTower != null && collidedTower == thisTowerNavInstance)
        {
            collidedTower.DestinationReached();
        }
    }

}
