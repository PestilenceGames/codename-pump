using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RangedBase : AttackPatternsBase
{
    [SerializeField] private ParticleSystem thisSystem;

    public override void AttackTarget()
    {
        base.AttackTarget();

        Debug.Log("TryingTo attack");
        if (targetToAttack != null)
        {
            if (targetToAttack.Dead)
                RemoveTarget(targetToAttack);
            else
                targetToAttack.TakeDamage(attackDamage);
        }
        
    }

}
