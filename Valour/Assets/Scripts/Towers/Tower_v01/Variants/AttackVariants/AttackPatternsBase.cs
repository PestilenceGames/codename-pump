using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AttackPatternsBase : MonoBehaviour
{
    // public float attackRange;
    [Header("TowerSettings")]
    public TowerBrain thisTowerBrain;
    [HideInInspector]
    public float attackDamage;
    [SerializeField] private float health;
    [SerializeField] private float attackRange;
    [SerializeField] private float attackCooldown;
    // [SerializeField] private TowerState thisTowerState;

    public SphereCollider attackRing;
    public ZombieBase targetToAttack;
    public TowerNav thisTowerNav;
    public VariantPicker thisTowerVariant;
    public List<ZombieBase> targets = new List<ZombieBase>();



    private ZombieBase tempTarget;
    private TowerTypeSO tempTowerSO;
    [SerializeField] private bool canAttack;
    [SerializeField] private bool attacking;

    private void OnEnable()
    {
        // thisTowerState = TowerState.Idle;

        attackRing = this.GetComponent<SphereCollider>();
        attackRing.isTrigger = true;

        thisTowerVariant = this.transform.parent.GetComponent<VariantPicker>();
        thisTowerVariant.RequestInitialization(this);

    }

    public void InitializeTower()
    {
        tempTowerSO = TowerManager.Instance.GetStatsOfType(thisTowerBrain.thisTowerType);

        // Debug.Log("loading in " + tempTowerSO);

        attackDamage = tempTowerSO.attackDamage;
        attackRange = tempTowerSO.attackRange;
        attackCooldown = tempTowerSO.attackCooldown;
        health = tempTowerSO.health;
        attackRing.radius = attackRange;

        thisTowerNav.SetUpAttackPatternsBase(this);
        attacking = false;

        CanAttack = true;

    }

    private void OnTriggerEnter(Collider other)
    {
        tempTarget = other.transform.GetComponent<ZombieBase>();

        if (tempTarget != null)
            targets.Add(tempTarget);
        UpdateTargets();
    }

    private void OnTriggerExit(Collider other)
    {
        tempTarget = other.transform.GetComponent<ZombieBase>();
        RemoveTarget(tempTarget);
    }

    public void RemoveTarget(ZombieBase zombie)
    {
        if (zombie != null)
        {
            if (targets.Contains(zombie))
            {
                targets.Remove(zombie);
            }
        }
        UpdateTargets();
    }

    public void UpdateTargets()
    {
        if (targets.Count > 0)
        {
            targetToAttack = targets[0];

            if (!attacking)
            {
                attacking = true;
                StartCoroutine(Attack());
            }

        }
        else
        {
            // StopCoroutine(Attack());
            // attacking = false;
        }

    }

    public bool CanAttack
    {
        get { return canAttack; }

        set
        {
            canAttack = value;
            // attackRing.enabled = canAttack;

            if (canAttack)
                attackRing.radius = attackRange;                
            else
                attackRing.radius = 0;
            UpdateTargets();
        }
    }

    IEnumerator Attack()
    {   attacking = true;
        while (canAttack)
        {
            AttackTarget();
            yield return new WaitForSeconds(attackCooldown);
        }

    }

    public virtual void AttackTarget()
    {
        Debug.Log("Attack");
        thisTowerNav.transform.LookAt(targetToAttack.transform, Vector3.up);
    }
}

public enum TowerState
{
    Idle,
    Attacking,
    Dying
}
