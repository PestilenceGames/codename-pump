using UnityEngine;

[CreateAssetMenu(fileName = "TowerTypeSO", menuName = "TowerTypeSO", order = 0)]
public class TowerTypeSO : ScriptableObject
{
    public TowerType towerType;

    [Header("Level One")]
    public int cost;
    public float health;
    public float attackRange;
    public float attackDamage;
    public float attackCooldown;

    // [Header("Level Two")]
    // public int costL02;
    // public float healthL02;
    // public float attackRangeL02;
    // public float attackDamageL02;
    // public float attackCooldownL02;

}
