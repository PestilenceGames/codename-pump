using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TowerManager : MonoBehaviour
{
    public static TowerManager Instance;

    [Header("Tower Settings")]
    public float deAggroRange;
    public LayerMask towerLayer;
    public LayerMask towerRingsLayer;
    public List<TowerTypeSO> towerTypeSOs = new List<TowerTypeSO>();


    [Header("Spawning")]
    [SerializeField] private Transform spawnPos;
    [SerializeField] private ObjPoolTowerV02 objPoolTowers;

    private TowerTypeSO tempTower;
    private GameObject tempTowerGO;

    private void Awake()
    {
        Instance = this;
    }

    public TowerTypeSO GetStatsOfType(TowerType type)
    {
        foreach (TowerTypeSO _type in towerTypeSOs)
        {
            if (_type.towerType == type)
                tempTower = _type;
        }
        return tempTower;
    }

    public bool CanSpawnTower()
    {
        bool _clearToSpawn = false;
        RaycastHit hit;

        if (Physics.SphereCast(spawnPos.position + Vector3.up* 2, 0.2f, Vector3.down, out hit, 2f, towerLayer))
        {
            _clearToSpawn = false;
            // TODO add must clear first prompt
        }
        else
            _clearToSpawn = true;

        return _clearToSpawn;
    }

    public void SpawnUnit()
    {
        Debug.Log("Spawn Unit");
        
        tempTowerGO = objPoolTowers.GetTower();
        tempTowerGO.transform.position = spawnPos.position;
        tempTowerGO.gameObject.SetActive(true);

        tempTowerGO.transform.GetChild(0).GetComponent<TowerAI>().Spawn(spawnPos.position);
    }

    public void ReplaceTower(TowerAI tower)
    {
        objPoolTowers.ReturnTower(tower.gameObject);
    }

    


}
