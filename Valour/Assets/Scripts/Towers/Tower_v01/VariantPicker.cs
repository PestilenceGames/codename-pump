using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VariantPicker : MonoBehaviour
{

    [SerializeField] private List<GameObject> variants = new List<GameObject>();
    [SerializeField] private TowerNav thisTowerNav;
    [SerializeField] private TowerBrain thisTowerBrain;
    private GameObject tempVariant;
    private TowerTypeSO towerTypeSO;

    public void SetupVariantPicker()
    {
        DisableAllVariants();
    }

    public TowerType TowerVariant // To update on change
    {
        set
        {
            SwitchTowerVariantTo(value);
        }
    }

    private GameObject FindActiveVariant()
    {
        tempVariant = null;
        foreach (GameObject _variant in variants)
        {
            if (_variant.activeSelf)
                tempVariant = _variant;
        }
        return tempVariant;
    }

    public void SwitchTowerVariantTo(TowerType type)
    {
        DisableAllVariants();
        if ((int)type < 8)
            variants[(int)type].SetActive(true);
        else
        {
            switch (type)
            {
                case TowerType.Archer02:
                    variants[(int)TowerType.Archer].SetActive(true);
                    break;
                case TowerType.Mage02:
                    variants[(int)TowerType.Mage].SetActive(true);
                    break;
                case TowerType.Infantry02:
                    variants[(int)TowerType.Infantry].SetActive(true);
                    break;
                case TowerType.Lancer02:
                    variants[(int)TowerType.Lancer].SetActive(true);
                    break;
                case TowerType.Knight02:
                    variants[(int)TowerType.Knight].SetActive(true);
                    break;
                default:
                    break;
            }
        }

    }

    private void DisableAllVariants()
    {
        foreach (GameObject variant in variants)
        {
            variant.SetActive(false);
        }
    }

    public void RequestInitialization(AttackPatternsBase request)
    {
        request.thisTowerVariant = this;
        // Redundant

        
        request.thisTowerNav = thisTowerNav;
        StartCoroutine(WaitToInitialize(request));
    }

    IEnumerator WaitToInitialize(AttackPatternsBase request)
    {
        yield return new WaitForEndOfFrame();
        request.InitializeTower();
    }
}

