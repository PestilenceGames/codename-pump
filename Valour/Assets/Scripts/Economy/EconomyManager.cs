using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EconomyManager : MonoBehaviour
{
    public static EconomyManager Instance;

    [Header("Economy Settings")]
    public int gold = 1000;


    private void Awake()
    {
        Instance = this;
    }

    private void Start()
    {
        EconomyUI.Instance.DisplayGold(gold);
    }

    public bool CanThisBeAfforded(int amount)
    {
        bool _affordable = (gold > amount) ? true : false;
        return _affordable;
    }

    public void PurchaseThis(int amount)
    {
        Gold = -amount;
    }

    public int Gold
    {
        get { return gold; }

        set
        {
            gold += value;
            EconomyUI.Instance.DisplayGold(gold);
        }
    }


}
