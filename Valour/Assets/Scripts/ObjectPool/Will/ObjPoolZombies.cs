using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjPoolZombies : MonoBehaviour
{
    [Header("Towers")]
    // public ObjPoolZombies Instance;
    [SerializeField] private GameObject zombie;
    [SerializeField] private int poolSize;
    [SerializeField] private List<GameObject> zombies = new List<GameObject>();
    private GameObject currentZombie;

    private void Start()
    {
        for (var i = 0; i < poolSize; i++)
        {
            currentZombie = Instantiate(zombie, Vector3.zero, zombie.transform.rotation);
            currentZombie.transform.name = $"ZomClone {i}";
            currentZombie.SetActive(false);
            currentZombie.transform.parent = this.transform;
            zombies.Add(currentZombie);
        }
    }

    public GameObject GetZombie()
    {
        bool _foundZombie = false;

        for (var i = 0; i < zombies.Count; i++)
        {
            if (!zombies[i].activeSelf)
            {
                _foundZombie = true;
                currentZombie = zombies[i];
                break;
            }
        }

        if (!_foundZombie)
        {
            currentZombie = Instantiate(zombie, Vector3.zero, zombie.transform.rotation);
            currentZombie.transform.name = $"ZomClone {zombies.Count}";
            currentZombie.SetActive(false);
            currentZombie.transform.parent = this.transform;
            zombies.Add(currentZombie);
        }

        return currentZombie;
    }

    public void ReturnZombie(GameObject zombie)
    {
        if (zombies.Contains(zombie))
            zombies.Remove(zombie);

        zombie.SetActive(false);
        zombies.Add(zombie);

    }


}
