using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjPoolTowers : MonoBehaviour
{
    [Header("Towers")]
    [SerializeField] private GameObject tower;
    [SerializeField] private int poolSize;
    [SerializeField] private List<GameObject> towers = new List<GameObject>();
    private GameObject currentTower;
    private int currentIndex;

    private void Start()
    {
        for (var i = 0; i < poolSize; i++)
        {
            currentTower = Instantiate(tower, Vector3.zero, tower.transform.rotation);
            currentTower.SetActive(false);
            currentTower.transform.parent = this.transform;
            towers.Add(currentTower);
        }
        currentIndex = 0;
    }

    public GameObject GetTower()
    {
        Debug.Log("In the market for a tower");
        currentTower = towers[currentIndex];
        currentIndex++;
        return currentTower;
    }

    public void ReturnTower(GameObject tower)
    {
                
    }



}
