using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjPoolTowerV02 : MonoBehaviour
{
    [Header("Towers")]
    // public ObjPoolZombies Instance;
    [SerializeField] private GameObject tower;
    [SerializeField] private int poolSize;
    [SerializeField] private List<GameObject> towers = new List<GameObject>();
    private GameObject currentTower;

    private void Start()
    {
        for (var i = 0; i < poolSize; i++)
        {
            currentTower = Instantiate(tower, Vector3.zero, tower.transform.rotation);
            currentTower.transform.name = $"ZomClone {i}";
            currentTower.SetActive(false);
            currentTower.transform.parent = this.transform;
            towers.Add(currentTower);
        }
    }

    public GameObject GetTower()
    {
        bool _foundTower = false;

        for (var i = 0; i < towers.Count; i++)
        {
            if (!towers[i].activeSelf)
            {
                _foundTower = true;
                currentTower = towers[i];
                break;
            }
        }

        if (!_foundTower)
        {
            currentTower = Instantiate(tower, Vector3.zero, tower.transform.rotation);
            currentTower.transform.name = $"Tower {towers.Count}";
            currentTower.SetActive(false);
            currentTower.transform.parent = this.transform;
            towers.Add(currentTower);
        }

        return currentTower;
    }

    public void ReturnTower(GameObject tower)
    {
        if (towers.Contains(tower))
            towers.Remove(tower);

        tower.SetActive(false);
        towers.Add(tower);

    }
}
