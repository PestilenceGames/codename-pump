using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class HealthBase : MonoBehaviour
{
    [Header("Health")]
    public float maxHealth;
    public float currentHealth;

    // [Header("Aggro")]
    // public float deAggroRange;
    // public LayerMask zombieLayer = 8;
    private ZombieBase tempZombie;

    private void OnEnable()
    {
        SetUpHealth();
    }

    public void SetUpHealth()
    {
        currentHealth = maxHealth;
    }

    public virtual void Die()
    {
        #region De Aggro Nearby Zombies

        Collider[] _zombies = Physics.OverlapSphere(this.transform.position, TowerManager.Instance.deAggroRange, EnemyManager.Instance.zombieLayer);

        foreach (Collider zombie in _zombies)
        {
            tempZombie = zombie.transform.GetComponent<ZombieBase>();
            if (tempZombie != null)
            {
                tempZombie.RemoveTarget(this);
            }
        }
        
        #endregion
    }

    public virtual void TakeDamage(float amount)
    {
        Health = -amount;
    }

    public float Health
    {
        get { return currentHealth; }

        set
        {
            currentHealth += value;

            if (currentHealth <= 0)
                Die();
        }
    }


}
