using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LoseStateObject : HealthBase
{

    public override void Die()
    {
        GameManager.Instance.LoseState();
        Debug.Log("Game Lost");
        base.Die();
    }

}
