using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DisolveOnDeath : MonoBehaviour
{

    [SerializeField] private Material thisMaterial;
    [SerializeField] private float dissolveTime;
    [SerializeField] private float dissolveRate = 0.05f;
    void Start()
    {
        // StartCoroutine(Dissolve(0));
        DissolveIn();
        StartCoroutine(WaitToDissolve());
    }

    private void DissolveIn()
    {
        thisMaterial.SetFloat("_Dissolve", 1);
        StartCoroutine(Dissolve(0));
    }

    IEnumerator WaitToDissolve()
    {
        yield return new WaitForSeconds(2);
        DissolveOut();
    }

    private void DissolveOut()
    {
        thisMaterial.SetFloat("_Dissolve", 0);
        StartCoroutine(Dissolve(1));

    }

    IEnumerator Dissolve(float direction)
    {
        float _timeleft = dissolveTime;

        while (_timeleft > 0)
        {
            _timeleft -= Time.deltaTime;
            yield return new WaitForFixedUpdate();

            thisMaterial.SetFloat("_Dissolve", Mathf.Lerp(thisMaterial.GetFloat("_Dissolve"), direction, dissolveRate));
        }
    }
}
