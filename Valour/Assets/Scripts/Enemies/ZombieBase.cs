using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class ZombieBase : HealthBase
{

    [SerializeField] private AttackRing thisRing;
    [SerializeField] EnemyStates currentState;
    [SerializeField] List<HealthBase> targets = new List<HealthBase>();
    private HealthBase tower;
    private NavMeshAgent thisAgent;
    private HealthBase currentTarget;
    private AttackAI tempAttackAI;
    private bool alive;

    private void OnEnable()
    {
        StartCoroutine(WaitForStartup());
    }

    IEnumerator WaitForStartup()
    {
        yield return new WaitForEndOfFrame();

        InitializeZombieClient();
    }

    public void InitializeZombieClient()
    {
        thisAgent = this.transform.GetComponent<NavMeshAgent>();
        thisAgent.enabled = true;
        thisAgent.destination = EnemyManager.Instance.keepPosition;

        thisRing.enabled = true;
        thisRing.InitializeRing();

        maxHealth = EnemyManager.Instance.zombieHealth;
        SetUpHealth();

        alive = true;
    }

    // public override void Die()
    // {
    //     base.Die();
    //     // TODO Kill the zombie
    // }


    internal void AttackStart()
    {
        // TODO Damage Tower
        // EnemyManager.Instance.DisplayBiteAt(this.transform.position);
    }

    private void SetNewDestination()
    {
        if (targets.Count > 0)
        {
            // int _random = UnityEngine.Random.Range(0, targets.Count);
            // thisAgent.destination = targets[_random].towerTransform.position;

            thisAgent.destination = targets[0].transform.position;
            
            currentTarget = targets[0];

            State = EnemyStates.Biting;
        }
        else
        {
            thisAgent.destination = EnemyManager.Instance.keepPosition;
            State = EnemyStates.Shambling;
        }
    }

    public HealthBase Target
    {
        get { return currentTarget; }
    }

    public EnemyStates State
    {
        get { return currentState; }

        set
        {
            currentState = value;
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        tower = other.transform.GetComponent<HealthBase>();

        if (tower != null)
        {
            targets.Add(tower);
            SetNewDestination();
        }

    }

    private void OnTriggerExit(Collider other)
    {
        tower = other.transform.GetComponent<HealthBase>();
        if (tower != null)
        {
            RemoveTarget(tower);
        }
    }

    public void RemoveTarget(HealthBase tower)
    {
        targets.Remove(tower);
        SetNewDestination();
    }

    public override void Die()
    {
        alive = false;

        if (thisAgent.enabled)
            thisAgent.isStopped = true;

        Debug.Log("Ded");
        // TODO Add death animation.

        targets.Clear();
        DeAggroTowers();

        // thisRing.enabled = false;
        EconomyManager.Instance.Gold = 1;
        
        EnemyManager.Instance.objPoolZombies.ReturnZombie(this.gameObject);
        // this.transform.position = Vector3.zero;
        // this.transform.gameObject.SetActive(false);

    }

    public bool Dead
    {
        get {return !alive;}
    }

    private void DeAggroTowers()
    {
        Collider[] _towerColliders = Physics.OverlapSphere(this.transform.position, 10f, TowerManager.Instance.towerRingsLayer);

        foreach (Collider _tower in _towerColliders)
        {
            tempAttackAI = _tower.transform.GetComponent<AttackAI>();

            if (tempAttackAI != null)
                tempAttackAI.RemoveTarget(this);
        }
    }

    public override void TakeDamage(float amount)
    {
        base.TakeDamage(amount);
    }
}

public enum EnemyStates
{
    Shambling,
    Biting,
    Dying,
    Waiting
}

// TODO Implement struct

// [System.Serializable]
// public struct TowerID
// {
//     // public 

//     public Transform towerTransform;
//     public HealthBase towerScript;

//     public TowerID(Transform towerTransform, HealthBase towerScript)
//     {
//         this.towerTransform = towerTransform;
//         this.towerScript = towerScript;
//     }
// }
