using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyManager : MonoBehaviour
{
    public static EnemyManager Instance;

    [Header("Zombie Settings")]
    public LayerMask zombieLayer;
    public float attackRange;
    public int attackDamage;
    public float attackCooldown;
    public float zombieHealth;

    [Header("Keep Settings")]
    [SerializeField] private Transform keep;
    [HideInInspector]
    public Vector3 keepPosition;

    [Header("Bite Effects")]
    [SerializeField] private ParticleSystem biteParticles;
    private Transform biteParticleTransfrom;

    [Header("Zombies Spawn")]
    [SerializeField] private bool spawnZombies;
    public ObjPoolZombies objPoolZombies;
    [SerializeField] private float secondsToAddNewSpawnPoint;
    [SerializeField] private List<Transform> spawnPoints = new List<Transform>();
    private List<Transform> activeSpawnPoints = new List<Transform>();


    private Transform zomSpawn;
    private GameObject tempZombie;



    private void Awake()
    {
        Instance = this;
        keepPosition = keep.position;

        biteParticleTransfrom = biteParticles.transform;
    }

    private void Start()
    {
        if (spawnZombies)
        {
            StartCoroutine(SpawnZombies());
        }

        StartCoroutine(WaitToAddSpawnPoint());
    }

    public void DisplayBiteAt(Vector3 location)
    {
        biteParticleTransfrom.position = new Vector3(location.x, location.y + 2, location.z);
        biteParticles.Emit(1);
        // Debug.Log("NOM");
    }

    public void InitializeZombieParent()
    {

    }

    IEnumerator SpawnZombies()
    {
        float spawnTimer = 5f;
        while (true)
        {
            yield return new WaitForSeconds(spawnTimer);
            spawnTimer = spawnTimer / 1.01f;
            SpawnZombie();
        }
    }

    public void SpawnZombie()
    {
        zomSpawn = PickSpawnPoint();
        // Debug.Log("Spawn Unit");

        tempZombie = objPoolZombies.GetZombie();
        tempZombie.transform.position = zomSpawn.position;
        tempZombie.gameObject.SetActive(true);

        tempZombie.transform.GetComponent<ZombieBase>().InitializeZombieClient();
    }

    private Transform PickSpawnPoint()
    {
        int _random = UnityEngine.Random.Range(0, activeSpawnPoints.Count);
        return spawnPoints[_random];
    }

    IEnumerator WaitToAddSpawnPoint()
    {
        int _index = 0;
        activeSpawnPoints.Clear();

        while (activeSpawnPoints.Count < spawnPoints.Count)
        {
            Debug.Log("Run");
            yield return new WaitForSeconds(secondsToAddNewSpawnPoint);
            activeSpawnPoints.Add(spawnPoints[_index]);
            Debug.Log(activeSpawnPoints.Count);
            _index++;
        }
    }

}
