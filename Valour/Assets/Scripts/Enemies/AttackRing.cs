using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AttackRing : MonoBehaviour
{

    [SerializeField] private ZombieBase zombieBase;
    [SerializeField] private float attackCooldown;
    [SerializeField] private int attackDamage;
    private SphereCollider thisRing;
    private HealthBase tempTower;


    public void InitializeRing()
    {
        thisRing = this.transform.GetComponent<SphereCollider>();
        thisRing.radius = EnemyManager.Instance.attackRange;
        
        attackDamage = EnemyManager.Instance.attackDamage;
        attackCooldown = EnemyManager.Instance.attackCooldown;
    }

    private void OnTriggerEnter(Collider other)
    {
        tempTower = other.transform.GetComponent<HealthBase>();

        if (zombieBase.State == EnemyStates.Biting && zombieBase.Target == tempTower)
        {
            Attack();
        }
    }

    private void Attack()
    {
        // zombieBase.AttackStart();

        StartCoroutine(AttackCooldown());

        EnemyManager.Instance.DisplayBiteAt(this.transform.position);
        thisRing.enabled = false;
        tempTower.TakeDamage(attackDamage);

        // if (tempTower.Health <= 0)
        // {
        //     zombieBase.RemoveTarget(new TowerID(tempTower.transform, tempTower));
        // }
    }

    private IEnumerator AttackCooldown()
    {
        yield return new WaitForSeconds(attackCooldown);
        // zombieBase.AttackOver();
        thisRing.enabled = true;
    }

 
}
