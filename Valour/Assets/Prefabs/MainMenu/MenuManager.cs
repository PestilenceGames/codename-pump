using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class MenuManager : MonoBehaviour
{
    [Tooltip("This scene will be loaded after pressing Start")]
    [SerializeField] private string sceneToLoad;

    [Header("Fade Settings")]
    [SerializeField] private Image fadePanel;
    [SerializeField] private float timeBeforeFadingOut;
    [SerializeField] private float fadeDuration;

    private void Start()
    {
        fadePanel.gameObject.SetActive(false);
    }

    public void StartGame()
    {
        StartCoroutine(Fade());
    }

    IEnumerator Fade()
    {
        yield return new WaitForSeconds(timeBeforeFadingOut);
        fadePanel.gameObject.SetActive(true);
        fadePanel.CrossFadeAlpha(0,0, false);
        fadePanel.CrossFadeAlpha(1, fadeDuration, false);
        yield return new WaitForSeconds(fadeDuration);
        SceneManager.LoadScene(sceneToLoad, LoadSceneMode.Single);
    }
    public void QuitGame()
    {
        Debug.Log("Terminate the game.");
        Application.Quit();
    }

}
